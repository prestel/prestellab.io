
// INSERT NAVIGATION BAR AT THE SIDE

var sidenavbar = '<div class="sidenavbar"><br>';
sidenavbar += '<img src="stefan-pic-circ.png" style="margin-left:auto;margin-right:auto;width:135px;height:135px;"> </img><br>';
sidenavbar += '&nbsp; &nbsp; <a href="https://gitlab.com/prestel">@gitlab</a> <br>';
sidenavbar += '&nbsp; &nbsp; <a href="https://inspirehep.net/authors/1058764?ui-citation-summary=true">@inspire</a> <br>';
sidenavbar += '&nbsp; &nbsp; <a href="https://se.linkedin.com/in/stefan-prestel?trk=public_profile_samename-profile">@linkedin</a> <br>';
sidenavbar += '</div>';

// add navigation bar to the top of the body
document.getElementsByTagName('body').item(0).insertAdjacentHTML('afterbegin',
  sidenavbar);

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}


var colltoo = document.getElementsByClassName("collapsible-too");
var j;

for (j = 0; j < colltoo.length; j++) {
  colltoo[j].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}

// INSERT LAST EDITED AT THE BOTTOM

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
var dLM   = new Date(document.lastModified);
var year  = dLM.getFullYear();
var month = monthNames[dLM.getMonth()];
var day  = dLM.getDate();
if (day<10) day = "0"+day;
document.getElementsByTagName('body').item(0).insertAdjacentHTML('beforeend',
  '<div class="lastedited">Last Edited: '+month+' '+day+', '+year+'</div>');

